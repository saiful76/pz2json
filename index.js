const proxy = require('express-http-proxy')
const parser = require('fast-xml-parser')
const he = require('he')
const cors = require('cors')
const app = require('express')()
const port = 3010

app.use(cors())

app.use('/api', proxy('localhost:8004', {
    filter: function (req) {
        return req.method === 'GET'
    },
    userResHeaderDecorator(headers) {
        headers['Content-Type'] = 'application/json'
        return headers
    },
    userResDecorator: function (_, proxyResData) {
        return parser.parse(proxyResData.toString(), {
            ignoreAttributes: false,
            tagValueProcessor: (val, _) => he.decode(val)
        })
    }
}))

app.listen(port, () => {
    console.log(`App listening at http://localhost:${port}`)
})